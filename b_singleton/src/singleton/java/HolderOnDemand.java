package singleton.java;

public class HolderOnDemand {
	static class Holder {
		private static HolderOnDemand instance = new HolderOnDemand();
	}
	
	public static HolderOnDemand getInstance() {
		return Holder.instance;
	}
}
