package chainofresponsibility.java;

import java.util.HashMap;
import java.util.Map;

public abstract class RequestHandler {
    RequestHandler next;

    public void setNext(RequestHandler next) {
        this.next = next;
    }

    abstract void handle(Map<String, String> request);
}

class VerifyHasName extends RequestHandler {

    @Override
    void handle(Map<String, String> request) {
        System.out.println("Verifying has name.");
        if(request.containsKey("name")){
            next.handle(request);
        }
    }
}
class VerifyNameLongerthan3 extends RequestHandler {

    @Override
    void handle(Map<String, String> request) {
        System.out.println("Verifying name length longer than 3.");
        if(request.get("name").length() > 3){
            next.handle(request);
        }
    }
}


class Client {
    public static void main(String[] args) {
        HashMap<String, String> request = new HashMap<>();
        request.put("name", "laozhu");

        RequestHandler verifiers = new VerifyHasName();
        verifiers.setNext(new VerifyNameLongerthan3());
        verifiers.handle(request);

        HashMap<String, String> request2 = new HashMap<>();
        request.put("name", "laozhu");
        verifiers.handle(request2);
    }
}
