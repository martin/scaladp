package adapter.scala.partial

trait MouseListener {
  def onLButtonDown() {}
  def onLButtonUp() {}
  def onMouseMove() {}
}

class Client extends MouseListener{
  override def onLButtonDown() {
    super.onLButtonDown()
  }
}


