trait CarElement

case class Body() extends CarElement
case class Engine() extends CarElement
case class Wheel(name: String) extends CarElement
case class Car(elems: CarElement*) extends CarElement

def printCarElement[T <: CarElement](target: T) {
  target match {
    case Car(elems@_*) => elems.foreach(printCarElement)
    case Body() => println("visiting body")
    case Engine() => println("visiting engine")
    case Wheel(name) => println("visiting " + name + " wheel")
  }
}

def doCarElement[T <: CarElement](target: T) {
  target match {
    case Car(elems@_*) => println("Starting my car")
    case Body() => println("Moving my body")
    case Engine() => println("Starting my engine")
    case Wheel(name) => println("Kicking my " + name + " wheel")
  }
}

val car = Car(
  Wheel("front left"), Wheel("front right"), Wheel("back left"), Wheel("back right"),
  Body(), Engine()
)

printCarElement(car)
doCarElement(car)

